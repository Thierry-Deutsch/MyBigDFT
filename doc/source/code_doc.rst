.. _api:

Code Documentation
------------------

This section focuses on the documentation of the MyBigDFT module and its API.
You will find here the documentation for the most useful classes and methods.

.. toctree::
    :maxdepth: 1

    iofiles
    job
    workflows
